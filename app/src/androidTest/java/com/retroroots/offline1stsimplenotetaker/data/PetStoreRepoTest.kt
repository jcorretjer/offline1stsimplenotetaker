package com.retroroots.offline1stsimplenotetaker.data

import com.retroroots.network.data_source.remote.state.ResponseState
import com.retroroots.offline1stsimplenotetaker.PetStoreDataTest
import com.retroroots.offline1stsimplenotetaker.data.local.PetStoreLocalDataSource
import com.retroroots.offline1stsimplenotetaker.data.model.pet.Pet
import com.retroroots.offline1stsimplenotetaker.data.remote.PetStoreRemoteDataSource
import com.retroroots.offline1stsimplenotetaker.data.repo.PetStoreRepo
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertNotNull
import junit.framework.TestCase.assertNull
import junit.framework.TestCase.assertTrue
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import javax.inject.Inject

@HiltAndroidTest
class PetStoreRepoTest : PetStoreDataTest
{
    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    private lateinit var petStoreRepo: PetStoreRepo

    @Inject
    lateinit var petStoreLocalDataSource: PetStoreLocalDataSource

    @Inject
    lateinit var petStoreRemoteDataSource: PetStoreRemoteDataSource

    @Before
    fun init()
    {
        hiltRule.inject()

        petStoreRepo = PetStoreRepo(petStoreRemoteDataSource, petStoreLocalDataSource)
    }

    @Test
    override fun selectByInvalidIdTest()
    {
        runBlocking {
            assertTrue(petStoreRepo.getPet(-1) is ResponseState.Failure)

            assertNull(petStoreLocalDataSource.selectById(-1))
        }
    }

    @Test
    override fun selectByValidIdTest()
    {
        runBlocking {
            //Should be consumed from api
            assertTrue(petStoreRepo.getPet(1) is ResponseState.Success)

            assertNotNull(petStoreLocalDataSource.selectById(1))

            //Should be consumed from local
            assertTrue(petStoreRepo.getPet(1) is ResponseState.Success)
        }
    }

    private var pet = Pet.emptyModel()

    @Test
    override fun createInvalidRecordTest()
    {
        runBlocking {
            assertTrue(petStoreRepo.createPet(pet) is ResponseState.Failure)

            assertNull(petStoreLocalDataSource.selectById(pet.id))
        }
    }

    @Test
    override fun createValidRecordTest()
    {
        runBlocking {
            assertTrue(petStoreRepo.createPet(pet.copy(id = 1, name = "test")) is ResponseState.Success)

            assertNotNull(petStoreLocalDataSource.selectById(1))
        }
    }

    @Test
    override fun updateInvalidRecordTest()
    {
        pet.copy(id = -1, name = "test").apply {
            runBlocking {
                assertTrue(petStoreRepo.updatePet(copy(name = "test2")) is ResponseState.Failure)

                assertNull(petStoreLocalDataSource.selectById(id))
            }
        }
    }

    @Test
    override fun updateValidRecordTest()
    {
        runBlocking {
            pet.copy(id = 1, name = "test").let { pet2 ->
                petStoreRepo.let { repo ->
                    repo.createPet(pet2)

                    assertTrue(repo.updatePet(pet2.copy(name = "test2")) is ResponseState.Success)

                    petStoreLocalDataSource.selectById(pet2.id).let {
                        assertNotNull(it)

                        assertEquals("test2", it!!.name)
                    }
                }
            }
        }
    }
}