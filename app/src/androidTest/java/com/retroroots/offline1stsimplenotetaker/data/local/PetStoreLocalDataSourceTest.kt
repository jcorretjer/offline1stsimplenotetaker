package com.retroroots.offline1stsimplenotetaker.data.local

import com.retroroots.offline1stsimplenotetaker.PetStoreDataTest
import com.retroroots.offline1stsimplenotetaker.data.model.pet.Pet
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import junit.framework.TestCase.assertNotNull
import junit.framework.TestCase.assertNull
import junit.framework.TestCase.assertTrue
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import javax.inject.Inject

@HiltAndroidTest
class PetStoreLocalDataSourceTest : PetStoreDataTest
{
    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Inject
    lateinit var petStoreLocalDataSource: PetStoreLocalDataSource

    @Before
    fun init()
    {
        hiltRule.inject()
    }

    @Test
    override fun selectByInvalidIdTest()
    {
        runBlocking {
                petStoreLocalDataSource.selectById(-1)?.let {
                    error("Should have failed test, but didn't")
                }
        }
    }

    @Test
    override fun selectByValidIdTest()
    {
        createRecord()

        runBlocking {
            assertNotNull(petStoreLocalDataSource.selectById(Pet.emptyModel().id))
        }
    }

    @Test
    override fun createInvalidRecordTest()
    {
        //No way to create an invalid record
    }

    private fun createRecord()
    {
        runBlocking {
            petStoreLocalDataSource.createByObj(Pet.emptyModel())
        }
    }

    @Test
    override fun createValidRecordTest()
    {
        createRecord()
    }

    @Test
    override fun updateInvalidRecordTest()
    {
        runBlocking {
            petStoreLocalDataSource.let {
                it.updateByObj(Pet.emptyModel().copy(name = "fake"))

                assertNull(it.selectById(Pet.emptyModel().id))
            }
        }
    }

    @Test
    override fun updateValidRecordTest()
    {
        createRecord()

        val newName = "fake"

        runBlocking {
            petStoreLocalDataSource.let {dataSource ->
                dataSource.updateByObj(Pet.emptyModel().copy(name = newName))

                dataSource.selectById(Pet.emptyModel().id).let {
                    assertNotNull(it)

                    assertTrue(it?.name == newName)
                }
            }
        }
    }
}