package com.retroroots.offline1stsimplenotetaker.data.local.di

import android.content.Context
import androidx.room.Room
import com.retroroots.offline1stsimplenotetaker.data.local.PetStoreDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import dagger.hilt.testing.TestInstallIn
import javax.inject.Singleton

@TestInstallIn(components = [SingletonComponent::class], replaces = [PetStoreDatabaseModule::class])
@Module
object PetStoreFakeDatabaseModule
{
    @Singleton
    @Provides
    fun providesPetStoreTestDatabase(@ApplicationContext context: Context) =
            Room.inMemoryDatabaseBuilder(context, PetStoreDatabase::class.java).build()
}