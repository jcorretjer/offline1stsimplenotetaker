package com.retroroots.offline1stsimplenotetaker.data.remote

import com.retroroots.network.data_source.remote.state.ApiError
import com.retroroots.network.data_source.remote.state.ResponseState
import com.retroroots.offline1stsimplenotetaker.PetStoreDataTest
import com.retroroots.offline1stsimplenotetaker.data.model.pet.Pet
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import junit.framework.TestCase
import junit.framework.TestCase.assertTrue
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import javax.inject.Inject

@HiltAndroidTest
class PetStoreRemoteDataSourceTest : PetStoreDataTest
{
    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Inject
    lateinit var petStoreRemoteDataSource: PetStoreRemoteDataSource

    @Before
    fun init()
    {
        hiltRule.inject()
    }

    @Test
    override fun selectByInvalidIdTest()
    {
        runBlocking {
            assertTrue((petStoreRemoteDataSource.selectById(-1) as ResponseState.Failure).error is ApiError.ServerError)
        }
    }

    @Test
    override fun selectByValidIdTest()
    {
        runBlocking {
            assertTrue(petStoreRemoteDataSource.selectById(1) is ResponseState.Success)
        }
    }

    @Test
    override fun createInvalidRecordTest()
    {
        runBlocking {
            // id Must be greater then 0 or the response will fail
            assertTrue(
                (petStoreRemoteDataSource.createByObj(
                    Pet.emptyModel().copy(id = -1, name = "failed")
                ) as ResponseState.Failure).error is ApiError.UnknownServerError
            )
        }
    }

    @Test
    override fun createValidRecordTest()
    {
        runBlocking {
            assertTrue(petStoreRemoteDataSource.createByObj(
                Pet.emptyModel().copy(id = 666, name = "success")
            ) is ResponseState.Success)
        }
    }

    @Test
    override fun updateInvalidRecordTest()
    {
        runBlocking {
            // id Must be greater then 0 or the response will fail
            assertTrue(
                (petStoreRemoteDataSource.updateByObj(
                    Pet.emptyModel().copy(id = -1, name = "failed")
                ) as ResponseState.Failure).error is ApiError.UnknownServerError
            )
        }
    }

    @Test
    override fun updateValidRecordTest()
    {
        runBlocking {
            (petStoreRemoteDataSource.createByObj(Pet.emptyModel().copy(id = 1337, name = "success")) as? ResponseState.Success)
                ?.let {
                    assertTrue(petStoreRemoteDataSource.updateByObj(it.data.copy(name = "s2")) is ResponseState.Success)
                }
                ?: run {
                    error("Expected response was success and the actual response was something else")
                }
        }
    }
}