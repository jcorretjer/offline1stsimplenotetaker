package com.retroroots.offline1stsimplenotetaker

interface PetStoreDataTest
{
    fun selectByInvalidIdTest()

    fun selectByValidIdTest()

    fun createInvalidRecordTest()

    fun createValidRecordTest()

    fun updateInvalidRecordTest()

    fun updateValidRecordTest()
}