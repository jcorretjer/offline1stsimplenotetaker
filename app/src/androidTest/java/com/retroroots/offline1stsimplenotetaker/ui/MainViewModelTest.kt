package com.retroroots.offline1stsimplenotetaker.ui

import com.retroroots.network.data_source.remote.state.ApiError
import com.retroroots.network.data_source.remote.state.ResponseState
import com.retroroots.offline1stsimplenotetaker.PetStoreDataTest
import com.retroroots.offline1stsimplenotetaker.data.model.pet.Pet
import com.retroroots.offline1stsimplenotetaker.data.repo.FakePetStoreRepo
import com.retroroots.offline1stsimplenotetaker.ui.main.PetStoreViewModel
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import junit.framework.TestCase.assertTrue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.TestScope
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
@HiltAndroidTest
class MainViewModelTest : PetStoreDataTest
{
    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    private val repo = FakePetStoreRepo()

    private lateinit var viewModel : PetStoreViewModel

    @Before
    fun init()
    {
        hiltRule.inject()
    }

    @Test
    override fun selectByInvalidIdTest()
    {
        runTest {
            fakeDataSourceBehaviors(
                this,
                {
                    viewModel.getPet(-1)
                },
                ResponseState.Failure(ApiError.UnknownServerError(Exception()))
            )

            assertTrue((viewModel.pet.value as ResponseState.Failure).error is ApiError.UnknownServerError)
        }
    }

    private lateinit var pet : Pet

    @Test
    override fun selectByValidIdTest()
    {
        pet = Pet.emptyModel().copy(id = 1)

        runTest{
            fakeDataSourceBehaviors(
                this,
                {
                    viewModel.getPet(1)
                },
                ResponseState.Success(pet)
            )

            assertTrue((viewModel.pet.value as ResponseState.Success).data.id == 1)
        }
    }

    @Test
    fun selectByValidIdFromLocalTest()
    {
        pet = Pet.emptyModel().copy(id = 1)

        runTest{
            fakeDataSourceBehaviors(
                this,
                {
                    viewModel.getPet(1)
                },
                ResponseState.Loading,
                pet
            )

            assertTrue((viewModel.pet.value as ResponseState.Success).data.id == 1)
        }
    }

    @Test
    override fun createInvalidRecordTest()
    {
        pet = Pet.emptyModel().copy(id = -1, name = "failed")

        runTest {
            fakeDataSourceBehaviors(
                this,
                {
                    viewModel.createPet(pet)
                },
                ResponseState.Failure(ApiError.UnknownServerError(Exception()))
            )

            assertTrue((viewModel.pet.value as ResponseState.Failure).error is ApiError.UnknownServerError)
        }
    }

    @Test
    override fun createValidRecordTest()
    {
        pet = Pet.emptyModel().copy(id = 666, name = "success")

        runTest {
            fakeDataSourceBehaviors(
                this,
                {
                    viewModel.createPet(pet)
                },
                ResponseState.Success(pet)
            )

            assertTrue((viewModel.pet.value as ResponseState.Success).data.id == 666)
        }
    }

    @Test
    override fun updateInvalidRecordTest()
    {
        pet = Pet.emptyModel().copy(id = -1, name = "failed")

        runTest {
            fakeDataSourceBehaviors(
                this,
                {
                    viewModel.updatePet(pet)
                },
                ResponseState.Failure(ApiError.UnknownServerError(Exception()))
            )

            assertTrue((viewModel.pet.value as ResponseState.Failure).error is ApiError.UnknownServerError)
        }
    }

    @Test
    override fun updateValidRecordTest()
    {
        pet = Pet.emptyModel().copy(id = 666, name = "success")

        runTest {
            fakeDataSourceBehaviors(
                this,
                {
                    viewModel.updatePet(pet)
                },
                ResponseState.Success(pet)
            )

            assertTrue((viewModel.pet.value as ResponseState.Success).data.id == 666)
        }
    }

    private fun assertDataStateIsLoading(testScope: TestScope)
    {
        viewModel = PetStoreViewModel(repo, StandardTestDispatcher(testScope.testScheduler))

        assertTrue(viewModel.pet.value is ResponseState.Loading)
    }

    private fun fakeDataSourceBehaviors(testScope: TestScope, onFakeRequest : () -> Unit, fakeResponse: ResponseState<Pet>, fakeResult : Pet? = null)
    {
        assertDataStateIsLoading(testScope)

        repo.fakeRoomResult(fakeResult)

        onFakeRequest()

        repo.fakeServerResponse(fakeResponse)

        testScope.advanceUntilIdle()
    }
}