package com.retroroots.offline1stsimplenotetaker.ui

import androidx.compose.ui.test.assertTextEquals
import androidx.compose.ui.test.isDisplayed
import androidx.compose.ui.test.isNotDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performClick
import androidx.compose.ui.test.performTextClearance
import androidx.compose.ui.test.performTextInput
import com.retroroots.network.data_source.remote.state.ApiError
import com.retroroots.network.data_source.remote.state.ResponseState
import com.retroroots.offline1stsimplenotetaker.data.model.pet.Pet
import com.retroroots.offline1stsimplenotetaker.data.repo.FakePetStoreRepo
import com.retroroots.offline1stsimplenotetaker.ui.composables.ScreenTestTag
import com.retroroots.offline1stsimplenotetaker.ui.main.MainActivity
import com.retroroots.offline1stsimplenotetaker.ui.main.MainUI
import com.retroroots.offline1stsimplenotetaker.ui.main.PetStoreViewModel
import com.retroroots.offline1stsimplenotetaker.ui.main.PrimaryScreen
import kotlinx.coroutines.Dispatchers
import org.junit.Rule
import org.junit.Test

class MainActivityTest
{
    private var pet = Pet.emptyModel().copy(id = 3366, name = "3366")

    @get:Rule
    val composeRule = createComposeRule()

    @Test
    fun loadingDisplaysTest()
    {
        composeRule.apply {
            setContent {
                PrimaryScreen(pet = ResponseState.Loading, petName = "", onPetNameChanged = {}, onCreateButtonClicked = {},{})
            }

            onNodeWithTag(ScreenTestTag.LOADING_SCREEN.value).isDisplayed()

            onNodeWithTag(ScreenTestTag.ERROR_SCREEN.value).isNotDisplayed()
        }
    }

    @Test
    fun errorDisplaysTest()
    {
        composeRule.apply {
            setContent {
                PrimaryScreen(
                    pet = ResponseState.Failure(ApiError.UnknownServerError(Exception("Oops! Broken..."))),
                    petName = "",
                    onPetNameChanged = {},
                    onCreateButtonClicked = {},
                    {})
            }

            onNodeWithTag(ScreenTestTag.LOADING_SCREEN.value).isNotDisplayed()

            onNodeWithTag(ScreenTestTag.ERROR_SCREEN.value).isDisplayed()
        }
    }

    @Test
    fun displaysResponseDataTest()
    {
        composeRule.apply {
            setContent {
                PrimaryScreen(
                    pet = ResponseState.Success(pet),
                    petName = "",
                    onPetNameChanged = {},
                    onCreateButtonClicked = {},
                    {})
            }

            onNodeWithTag(ScreenTestTag.LOADING_SCREEN.value).isNotDisplayed()

            onNodeWithTag(ScreenTestTag.ERROR_SCREEN.value).isNotDisplayed()

            onNodeWithTag(MainActivity.TestTag.PET_NAME_TXT_FD.name).isDisplayed()

            onNodeWithTag(MainActivity.TestTag.RESPONSE_TXT.name).assertTextEquals(pet.toString())
        }
    }

    private val fakePetStoreRepo = FakePetStoreRepo()

    @Test
    fun createPetRecordDisplaysCorrectDataTest()
    {
        val petName = "some dog"

        pet = pet.copy(id = 1, name = petName)

        fakePetStoreRepo.fakeServerResponse(ResponseState.Success(pet))

        composeRule.apply {
            setContent {

                MainUI(PetStoreViewModel(fakePetStoreRepo, Dispatchers.Main))
            }

            onNodeWithTag(MainActivity.TestTag.PET_NAME_TXT_FD.name).performTextInput(petName)

            onNodeWithTag(MainActivity.TestTag.CREATE_BTN.name).performClick()

            onNodeWithTag(MainActivity.TestTag.RESPONSE_TXT.name).assertTextEquals(pet.copy(name = petName).toString())
        }
    }

    @Test
    fun createUpdatePetRecordDisplaysCorrectDataTest()
    {
        var petName = "some dog"

        pet = pet.copy(id = 1, name = petName)

        fakePetStoreRepo.fakeServerResponse(ResponseState.Success(pet))

        composeRule.apply {
            setContent {

                MainUI(PetStoreViewModel(fakePetStoreRepo, Dispatchers.Main))
            }

            onNodeWithTag(MainActivity.TestTag.PET_NAME_TXT_FD.name).performTextInput(petName)

            onNodeWithTag(MainActivity.TestTag.CREATE_BTN.name).performClick()

            onNodeWithTag(MainActivity.TestTag.RESPONSE_TXT.name).assertTextEquals(pet.copy(name = petName).toString())

            petName = "doggy"

            onNodeWithTag(MainActivity.TestTag.PET_NAME_TXT_FD.name).performTextClearance()

            onNodeWithTag(MainActivity.TestTag.PET_NAME_TXT_FD.name).performTextInput(petName)

            pet = pet.copy(id = 1, name = petName)

            fakePetStoreRepo.fakeServerResponse(ResponseState.Success(pet))

            onNodeWithTag(MainActivity.TestTag.UPDATE_BTN.name).performClick()

            onNodeWithTag(MainActivity.TestTag.RESPONSE_TXT.name).assertTextEquals(pet.copy(name = petName).toString())
        }
    }
}