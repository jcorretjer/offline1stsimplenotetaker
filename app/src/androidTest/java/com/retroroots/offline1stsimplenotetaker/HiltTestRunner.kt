package com.retroroots.offline1stsimplenotetaker

import android.app.Application
import android.content.Context
import dagger.hilt.android.testing.HiltTestApplication
import androidx.test.runner.AndroidJUnitRunner

class HiltTestRunner: AndroidJUnitRunner() {
    override fun newApplication(cl: ClassLoader?, className: String?, context: Context?): Application =
            super.newApplication(cl, HiltTestApplication::class.java.name, context)

}