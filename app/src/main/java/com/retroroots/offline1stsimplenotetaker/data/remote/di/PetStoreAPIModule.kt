package com.retroroots.offline1stsimplenotetaker.data.remote.di

import com.retroroots.offline1stsimplenotetaker.data.remote.api.PetStoreAPI
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object PetStoreAPIModule
{
    @Singleton
    @Provides
    fun providesPetStoreAPIRetrofit(): Retrofit = Retrofit.Builder()
        .baseUrl(PetStoreAPI.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    @Singleton
    @Provides
    fun providesPetStoreAPI(retrofit: Retrofit): PetStoreAPI = retrofit.create(PetStoreAPI::class.java)
}