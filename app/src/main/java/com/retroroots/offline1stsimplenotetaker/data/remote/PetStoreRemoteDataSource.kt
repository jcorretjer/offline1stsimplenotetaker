package com.retroroots.offline1stsimplenotetaker.data.remote

import com.retroroots.network.data_source.remote.CompactRemoteDataSource
import com.retroroots.network.data_source.remote.state.ApiError
import com.retroroots.network.data_source.remote.state.ResponseState
import com.retroroots.offline1stsimplenotetaker.data.model.pet.Pet
import com.retroroots.offline1stsimplenotetaker.data.model.pet.RawPet
import com.retroroots.offline1stsimplenotetaker.data.remote.api.PetStoreAPI
import com.retroroots.util.ext.responseBodyToString
import org.json.JSONObject
import retrofit2.Response

enum class ErrorBodyKey(val value : String)
{
    MESSAGE("message")
}

class PetStoreRemoteDataSource(private val petStoreAPI: PetStoreAPI) : CompactRemoteDataSource<RawPet, Pet>()
{
    override suspend fun updateByObj(obj: Pet): ResponseState<Pet> =
            try
            {
                compressResponse(petStoreAPI.updatePet(obj.toInternalModel()))
            }

            catch (exception : Exception)
            {
                processException(exception)
            }

    override suspend fun selectById(id: Int): ResponseState<Pet> =
            try
            {
                compressResponse(petStoreAPI.getPet(id))
            }

            catch (exception : Exception)
            {
                processException(exception)
            }

    override suspend fun createByObj(obj: Pet): ResponseState<Pet> =
        try
        {
            compressResponse(petStoreAPI.createPet(obj.toInternalModel()))
        }

        catch (exception : Exception)
        {
            processException(exception)
        }

    override fun compressResponse(process: Response<RawPet>): ResponseState<Pet>
    {
        process.apply {
            body()?.let {
                if(isSuccessful)
                    return ResponseState.Success(it.toExternalModel())
            }

            JSONObject(errorBody()?.responseBodyToString() ?: "").getString(ErrorBodyKey.MESSAGE.value).let {
                    return ResponseState.Failure(ApiError.ServerError(Exception(it)))
                }
        }
    }
}