package com.retroroots.offline1stsimplenotetaker.data.model.pet

data class Category(
    val id: Int,
    val name: String
)