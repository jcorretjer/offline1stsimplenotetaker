package com.retroroots.offline1stsimplenotetaker.data.local.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.retroroots.offline1stsimplenotetaker.data.model.pet.Category
import com.retroroots.offline1stsimplenotetaker.data.model.pet.Pet

@Entity
data class PetEntity(
    val category: Category,
    /**
     * Must be greater then 0 or the response will fail
     */
    @PrimaryKey
    val id: Int,
    val name: String,
    val status: String
)
{
    fun toExternalModel() = Pet(category, id, name, status)
}