package com.retroroots.offline1stsimplenotetaker.data.local.di

import com.retroroots.offline1stsimplenotetaker.data.local.PetStoreDatabase
import com.retroroots.offline1stsimplenotetaker.data.local.PetStoreLocalDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object PetStoreLocalDataSourceModule
{
    @Singleton
    @Provides
    fun providesPetStoreLocalDataSource(petStoreDatabase: PetStoreDatabase) = PetStoreLocalDataSource(petStoreDatabase)
}