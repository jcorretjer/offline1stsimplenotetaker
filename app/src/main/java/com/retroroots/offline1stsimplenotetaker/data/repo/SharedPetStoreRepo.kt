package com.retroroots.offline1stsimplenotetaker.data.repo

import com.retroroots.network.data_source.remote.state.ResponseState
import com.retroroots.offline1stsimplenotetaker.data.model.pet.Pet

/**
 * This abstraction is only useful for writing view model test and previewing composable with view models
 */
interface SharedPetStoreRepo
{
    suspend fun getPet(id : Int) : ResponseState<Pet>

    suspend fun createPet(pet: Pet) : ResponseState<Pet>

    suspend fun updatePet(pet: Pet) : ResponseState<Pet>
}