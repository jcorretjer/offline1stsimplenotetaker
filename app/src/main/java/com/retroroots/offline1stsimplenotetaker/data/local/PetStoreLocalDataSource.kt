package com.retroroots.offline1stsimplenotetaker.data.local

import com.retroroots.network.data_source.local.LocalDataSource
import com.retroroots.offline1stsimplenotetaker.data.model.pet.Pet

class PetStoreLocalDataSource(petStoreDatabase: PetStoreDatabase) : LocalDataSource<Pet, Pet>
{
    private val dao = petStoreDatabase.petStoreDao()

    override suspend fun selectById(id : Int): Pet? =
            try
            {
                dao.getPet(id).toExternalModel()
            }

            catch (_ : Exception)
            {
                null
            }

    override suspend fun createByObj(obj: Pet)
    {
        dao.createPet(obj.toInternalEntity())
    }

    override suspend fun updateByObj(obj: Pet)
    {
        dao.updatePet(obj.toInternalEntity())
    }
}