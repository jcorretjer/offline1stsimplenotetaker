package com.retroroots.offline1stsimplenotetaker.data.local.model.converter

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.retroroots.offline1stsimplenotetaker.data.model.pet.Category

class CategoryConverter
{
    @TypeConverter
    fun fromCategory(category: Category): String = Gson().toJson(category)

    @TypeConverter
    fun jsonToCategory(json : String): Category = Gson().fromJson(json, Category::class.java)
}
