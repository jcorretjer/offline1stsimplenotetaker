package com.retroroots.offline1stsimplenotetaker.data.model.pet

data class RawPet(
    val category: Category,
    /**
     * Must be greater then 0 or the response will fail
     */
    val id: Int,
    val name: String,
    val photoUrls: List<String> = emptyList(),
    val status: String,
    val tags: List<Tag> = emptyList()
)
{
    fun toExternalModel() = Pet(category, id, name, status)
}