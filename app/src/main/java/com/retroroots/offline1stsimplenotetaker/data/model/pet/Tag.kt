package com.retroroots.offline1stsimplenotetaker.data.model.pet

data class Tag(
    val id: Int,
    val name: String
)