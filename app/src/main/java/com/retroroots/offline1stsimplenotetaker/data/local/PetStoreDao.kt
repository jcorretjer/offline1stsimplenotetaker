package com.retroroots.offline1stsimplenotetaker.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Update
import com.retroroots.offline1stsimplenotetaker.data.local.model.PetEntity

@Dao
interface PetStoreDao
{
    @androidx.room.Query("SELECT * FROM petentity WHERE id == :id")
    suspend fun getPet(id : Int) : PetEntity

    @Insert
    suspend fun createPet(pet: PetEntity)

    @Update
    suspend fun updatePet( pet: PetEntity)
}