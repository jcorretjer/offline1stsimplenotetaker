package com.retroroots.offline1stsimplenotetaker.data.repo

import com.retroroots.network.data_source.remote.state.ResponseState
import com.retroroots.offline1stsimplenotetaker.data.model.pet.Pet

/**
 * Use only for test and previewing composable with view models
 */
class FakePetStoreRepo : SharedPetStoreRepo
{
    private lateinit var petResponse : ResponseState<Pet>

    private var petResult : Pet? = null

    fun fakeServerResponse(value : ResponseState<Pet>)
    {
        petResponse = value
    }

    fun fakeRoomResult(value : Pet?)
    {
        petResult = value
    }

    override suspend fun getPet(id: Int): ResponseState<Pet> = petResult?.let {
        return ResponseState.Success(it)
    }?: run {
        petResponse
    }

    override suspend fun createPet(pet: Pet): ResponseState<Pet> = petResponse

    override suspend fun updatePet(pet: Pet): ResponseState<Pet> = petResponse
}