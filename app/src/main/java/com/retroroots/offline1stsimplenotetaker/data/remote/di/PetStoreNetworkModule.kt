package com.retroroots.offline1stsimplenotetaker.data.remote.di

import com.retroroots.offline1stsimplenotetaker.data.local.PetStoreLocalDataSource
import com.retroroots.offline1stsimplenotetaker.data.repo.PetStoreRepo
import com.retroroots.offline1stsimplenotetaker.data.remote.PetStoreRemoteDataSource
import com.retroroots.offline1stsimplenotetaker.data.remote.api.PetStoreAPI
import com.retroroots.offline1stsimplenotetaker.data.repo.SharedPetStoreRepo
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.Dispatchers
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object PetStoreNetworkModule
{
    @Singleton
    @Provides
    fun providesPetStoreRemoteDataSource(petStoreAPI: PetStoreAPI) = PetStoreRemoteDataSource(petStoreAPI)

    @Singleton
    @Provides
    fun providesPetStoreRepo(
        petStoreRemoteDataSource: PetStoreRemoteDataSource,
        petStoreLocalDataSource: PetStoreLocalDataSource
    ) : SharedPetStoreRepo =
            PetStoreRepo(petStoreRemoteDataSource, petStoreLocalDataSource)

    @Singleton
    @Provides
    fun providesIODispatcher() = Dispatchers.IO
}