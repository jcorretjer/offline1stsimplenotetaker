package com.retroroots.offline1stsimplenotetaker.data.remote.api

import com.retroroots.offline1stsimplenotetaker.BuildConfig
import com.retroroots.offline1stsimplenotetaker.data.model.pet.RawPet
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path

const val PET_PATH = "pet"

const val ID_PATH = "id"

interface PetStoreAPI
{
    companion object
    {
        const val BASE_URL = BuildConfig.PET_STORE_API_BASE_URL
    }

    @GET("$PET_PATH/{$ID_PATH}")
    suspend fun getPet(@Path(ID_PATH) id : Int) : Response<RawPet>

    @POST(PET_PATH)
    suspend fun createPet(@Body pet: RawPet) : Response<RawPet>

    @PUT(PET_PATH)
    suspend fun updatePet(@Body pet: RawPet) : Response<RawPet>
}