package com.retroroots.offline1stsimplenotetaker.data.repo

import com.retroroots.network.data_source.remote.state.ResponseState
import com.retroroots.offline1stsimplenotetaker.data.local.PetStoreLocalDataSource
import com.retroroots.offline1stsimplenotetaker.data.model.pet.Pet
import com.retroroots.offline1stsimplenotetaker.data.remote.PetStoreRemoteDataSource

class PetStoreRepo(
    private val petStoreRemoteDataSource: PetStoreRemoteDataSource,
    private val petStoreLocalDataSource: PetStoreLocalDataSource
) : SharedPetStoreRepo
{
    override suspend fun getPet(id : Int) =
        petStoreLocalDataSource.selectById(id)?.let {
            return ResponseState.Success(it)
        }?: run {
            petStoreRemoteDataSource.selectById(id).apply {
                if(this is ResponseState.Success)
                    petStoreLocalDataSource.createByObj(data)
            }
        }

    override suspend fun createPet(pet: Pet) = petStoreRemoteDataSource.createByObj(pet).apply {
        if(this is ResponseState.Success)
            petStoreLocalDataSource.createByObj(data)
    }

    override suspend fun updatePet(pet: Pet) = petStoreRemoteDataSource.updateByObj(pet).apply {
        if(this is ResponseState.Success)
            petStoreLocalDataSource.updateByObj(data)
    }
}