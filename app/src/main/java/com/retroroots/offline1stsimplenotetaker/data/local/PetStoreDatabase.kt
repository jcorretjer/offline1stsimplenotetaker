package com.retroroots.offline1stsimplenotetaker.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.retroroots.offline1stsimplenotetaker.data.local.model.PetEntity
import com.retroroots.offline1stsimplenotetaker.data.local.model.converter.CategoryConverter

@Database(entities = [PetEntity::class], version = 1, exportSchema = false)
@TypeConverters(CategoryConverter::class)
abstract class PetStoreDatabase : RoomDatabase()
{
    abstract fun petStoreDao() : PetStoreDao
}