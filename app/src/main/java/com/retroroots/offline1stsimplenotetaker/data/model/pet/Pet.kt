package com.retroroots.offline1stsimplenotetaker.data.model.pet

import com.retroroots.offline1stsimplenotetaker.data.local.model.PetEntity

data class Pet(
    val category: Category,
    /**
     * Must be greater then 0 or the response will fail
     */
    val id: Int,
    val name: String,
    val status: String
)
{
    fun toInternalModel() = RawPet(category, id, name, status = status)

    fun toInternalEntity() = PetEntity(category, id, name, status)

    companion object
    {
        fun emptyModel() = Pet(
            Category(-1, ""),
            -1,
            "",
            ""
        )
    }
}
