package com.retroroots.offline1stsimplenotetaker.data.local.di

import android.content.Context
import androidx.room.Room
import com.retroroots.offline1stsimplenotetaker.data.local.PetStoreDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object PetStoreDatabaseModule
{
    @Singleton
    @Provides
    fun providesPetStoreDatabase(@ApplicationContext context: Context) =
            Room.databaseBuilder(context, PetStoreDatabase::class.java, PetStoreDatabase::class.simpleName).build()
}