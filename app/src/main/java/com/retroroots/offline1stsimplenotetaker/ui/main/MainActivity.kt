package com.retroroots.offline1stsimplenotetaker.ui.main

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.viewmodel.compose.viewModel
import com.retroroots.network.data_source.remote.state.ApiError
import com.retroroots.network.data_source.remote.state.ResponseState
import com.retroroots.offline1stsimplenotetaker.R
import com.retroroots.offline1stsimplenotetaker.data.model.pet.Pet
import com.retroroots.offline1stsimplenotetaker.data.repo.FakePetStoreRepo
import com.retroroots.offline1stsimplenotetaker.ui.composables.ErrorScreen
import com.retroroots.offline1stsimplenotetaker.ui.composables.LoadingScreen
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

const val REQUEST_PET_ONCE = true

@AndroidEntryPoint
class MainActivity : ComponentActivity()
{
    enum class TestTag
    {
        PET_NAME_TXT_FD,
        RESPONSE_TXT,
        CREATE_BTN,
        UPDATE_BTN
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContent {
            MainUI()
        }
    }
}

@Composable
fun MainUI(petStoreViewModel: PetStoreViewModel = viewModel())
{
    val pet by petStoreViewModel.pet.collectAsStateWithLifecycle()

    var idCounter by remember {
        mutableIntStateOf(1)
    }

    var petName by remember {
        mutableStateOf("")
    }

    val coroutineScope = rememberCoroutineScope()

    MaterialTheme {
        PrimaryScreen(
            pet,
            petName,
            {
                petName = it
            },
            {
                coroutineScope.launch {
                    petStoreViewModel.createPet(
                        Pet.emptyModel()
                            .copy(id = idCounter, name = petName)
                    )

                    idCounter++
                }
            },
            {
                coroutineScope.launch {
                    (pet as? ResponseState.Success)?.let {
                        petStoreViewModel.updatePet(it.data.copy(name = petName))
                    }
                }
            })
    }

    LaunchedEffect(REQUEST_PET_ONCE) {
        petStoreViewModel.getPet(1)
    }
}

@Composable
fun PrimaryScreen(
    pet: ResponseState<Pet>,
    petName: String,
    onPetNameChanged: (String) -> Unit,
    onCreateButtonClicked: () -> Unit,
    onUpdateButtonClicked: () -> Unit
)
{
    Column(
        Modifier
            .fillMaxWidth()
            .background(Color.Blue)
            .padding(5.dp)
    )
    {
        SearchSection(
            petName,
            onPetNameChanged,
            onCreateButtonClicked,
            onUpdateButtonClicked
        )

        DataSection(pet)
    }
}

@Composable
fun PetNameTxtFd(text : String, onValueChanged: (String) -> Unit)
{
    TextField(
        text,
        onValueChanged,
        Modifier
            .fillMaxWidth()
            .testTag(MainActivity.TestTag.PET_NAME_TXT_FD.name),
        label = { Text(text = "Pet Name")},
        singleLine = true
    )
}

@Composable
fun DataSection(pet: ResponseState<Pet>)
{
    when (pet)
    {
        is ResponseState.Failure ->
        {
            ErrorScreen(
                pet.error.exception.message
                    ?: stringResource(id = R.string.data_not_available_txt)
            )
        }

        ResponseState.Loading -> LoadingScreen()

        is ResponseState.Success ->
        {
            ResponseTxt(pet.data.toString())
        }
    }
}

@Composable
fun ResponseTxt(text: String)
{
    Text(
        text,
        Modifier
            .fillMaxSize()
            .background(Color.Magenta)
            .testTag(MainActivity.TestTag.RESPONSE_TXT.name)
    )

}

@Composable
fun SearchSection(
    petName: String,
    onPetNameChanged: (String) -> Unit,
    onCreateButtonClicked: () -> Unit,
    onUpdateButtonClicked: () -> Unit
)
{
    PetNameTxtFd(petName, onPetNameChanged)

    Row(
        Modifier
            .fillMaxWidth()
            .background(Color.Green),
        horizontalArrangement = Arrangement.SpaceEvenly,
    ) {
        Button(
            onCreateButtonClicked,
            Modifier.testTag(MainActivity.TestTag.CREATE_BTN.name)
        )
        {
            Text("Create")
        }

        Button(
            onUpdateButtonClicked,
            Modifier.testTag(MainActivity.TestTag.UPDATE_BTN.name)
        )
        {
            Text("Update")
        }
    }
}

@Preview
@Composable
fun MainUIPreview()
{
    MainUI(PetStoreViewModel(FakePetStoreRepo(), Dispatchers.IO))
}

@Preview
@Composable
fun PrimaryScreenLoadingPreview()
{
    PrimaryScreen(
        pet = ResponseState.Loading,
        petName = "",
        onPetNameChanged = {},
        onCreateButtonClicked = {}) {

    }
}

@Preview
@Composable
fun PrimaryScreenErrorPreview()
{
    PrimaryScreen(
        pet = ResponseState.Failure(ApiError.ServerError()),
        petName = "",
        onPetNameChanged = {},
        onCreateButtonClicked = {}) {

    }
}

@Preview
@Composable
fun PrimaryScreenSuccessPreview()
{
    PrimaryScreen(
        pet = ResponseState.Success(Pet.emptyModel().copy(name = "boy")),
        petName = "",
        onPetNameChanged = {},
        onCreateButtonClicked = { }) {
    }
}

@Preview
@Composable
fun ResponseTxtFdPreview()
{
    ResponseTxt("Hello")
}

@Preview(showBackground = true)
@Composable
fun PetNameInputPreview()
{
    PetNameTxtFd("dog"
    ) {

    }
}

@Preview
@Composable
fun SearchScreenPreview()
{
    SearchSection(
        petName = "name",
        onPetNameChanged = {},
        onCreateButtonClicked = {}) {
    }
}

@Preview
@Composable
fun LoadingDataSectionPreview()
{
    DataSection(pet = ResponseState.Loading)
}

@Preview
@Composable
fun ErrorDataSectionPreview()
{
    DataSection(pet = ResponseState.Failure(ApiError.ServerError()))
}

@Preview
@Composable
fun SuccessDataSectionPreview()
{
    DataSection(pet = ResponseState.Success(Pet.emptyModel().copy(name = "buddy")))
}