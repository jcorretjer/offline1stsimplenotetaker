package com.retroroots.offline1stsimplenotetaker.ui.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.retroroots.network.data_source.remote.state.ResponseState
import com.retroroots.offline1stsimplenotetaker.data.model.pet.Pet
import com.retroroots.offline1stsimplenotetaker.data.repo.SharedPetStoreRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PetStoreViewModel @Inject constructor(private val repo: SharedPetStoreRepo, private val dispatcher: CoroutineDispatcher) : ViewModel()
{
    private val _pet : MutableStateFlow<ResponseState<Pet>> = MutableStateFlow(ResponseState.Loading)

    val pet : StateFlow<ResponseState<Pet>> = _pet

    fun getPet(id : Int)
    {
        _pet.value = ResponseState.Loading

        viewModelScope.launch(dispatcher) {
            _pet.value = repo.getPet(id)
        }
    }

    fun createPet(pet: Pet)
    {
        _pet.value = ResponseState.Loading

        viewModelScope.launch(dispatcher) {
            _pet.value = repo.createPet(pet)
        }
    }

    fun updatePet(pet: Pet)
    {
        _pet.value = ResponseState.Loading

        viewModelScope.launch(dispatcher) {
            _pet.value = repo.updatePet(pet)
        }
    }
}