package com.retroroots.offline1stsimplenotetaker.ui.composables

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.retroroots.offline1stsimplenotetaker.R
import com.retroroots.offline1stsimplenotetaker.ui.theme.TransparentWhite

enum class ScreenTestTag(val value : String)
{
    LOADING_SCREEN("LOADING_SCREEN"),
    ERROR_SCREEN("ERROR_SCREEN")
}

@Composable
fun LoadingScreen()
{
    Box(
        Modifier
            .fillMaxSize()
            .background(TransparentWhite)
            .testTag(ScreenTestTag.LOADING_SCREEN.value),
        contentAlignment = Alignment.Center
    )
    {
        CircularProgressIndicator(
            Modifier.width(60.dp)
        )
    }
}

@Preview
@Composable
fun LoadingScreenPreview()
{
    LoadingScreen()
}

@Composable
fun ErrorScreen(errorText : String)
{
    Column(
        Modifier
            .fillMaxSize()
            .testTag(ScreenTestTag.ERROR_SCREEN.value)
            .padding(10.dp)
            .background(Color.Gray),
        Arrangement.Center,
        Alignment.CenterHorizontally
    )
    {
        Image(
            painterResource(R.drawable.baseline_warning_amber_24),
            "",
            Modifier.width(100.dp)
                .height(100.dp)
        )

        Text(errorText, textAlign = TextAlign.Center)
    }
}

@Preview
@Composable
fun ErrorScreenNoDataTxtPreview()
{
    ErrorScreen(stringResource(id = R.string.data_not_available_txt))
}

@Preview
@Composable
fun ErrorScreenDummyTxtPreview()
{
    ErrorScreen("Something went wrong")
}

@Preview
@Composable
fun ErrorScreenLongTxtPreview()
{
    ErrorScreen("Something went wronggggggggggggggggggggggggggggggggggggggggggggggggggggggg")
}