package com.retroroots.offline1stsimplenotetaker

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class PetStoreApplication : Application()