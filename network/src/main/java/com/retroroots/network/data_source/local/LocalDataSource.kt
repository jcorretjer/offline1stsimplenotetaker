package com.retroroots.network.data_source.local

interface LocalDataSource<ArgumentType, ReturnType>
{
    suspend fun selectById(id : Int): ReturnType?

    suspend fun updateByObj(obj : ArgumentType)

    suspend fun createByObj(obj : ArgumentType)
}